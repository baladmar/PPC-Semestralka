#ifndef GPS_H
#define GPS_H
#include <QString>

class gps
{
public:

    gps();
    ~gps();

    QString dd_latitude="0",dd_longitude="0";

    QString speed="0",satellites="0",altitude="0",accuracy="0";

    void set_coordinates(QString latitude,QString longitude,QString altitude_input);
    void set_speed(QString input);
    void set_satellites(QString input);
    void set_altitude(QString input);
    void set_accuracy(QString input);
};

#endif // GPS_H
