#ifndef SENSORS_WIDGET_H
#define SENSORS_WIDGET_H

#include <QWidget>
#include <QFormLayout>
#include <QBoxLayout>
#include <QPushButton>
#include <QComboBox>
#include <QColorDialog>
#include <QLineEdit>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QPlainTextEdit>
#include <QLabel>
#include <QGroupBox>
#include <QCloseEvent>

#include <parse.h>
#include <gps.h>
#include <gl_widget.h>

class sensors : public QWidget
{
    Q_OBJECT
private:
    QBoxLayout * layout;

    QGroupBox * gps_box;
    QFormLayout * gps_widget_layout;
    QLineEdit *longitude_widget;
    QLineEdit *latitude_widget;
    QLineEdit *speed_widget;
    QLineEdit *altitude_widget;
    QLineEdit *accuracy_widget;
    QLineEdit *satellites_widget;
    gps gps_data;

    QGroupBox * other_sensor_box;
    QFormLayout * other_sensor_layout;
    QLineEdit *pressure_widget;
    QLineEdit *temperature_widget;

    QGroupBox * mpu_box;
    QGroupBox * mpu_box_gyro;
    QGroupBox * mpu_box_accel;
    QGroupBox * mpu_box_magneto;
    QFormLayout * mpu_layout;
    QFormLayout * mpu_widget_layout[3];
    QLineEdit *mpu_gyro[3];
    QLineEdit *mpu_accel[3];
    QLineEdit *mpu_magneto[3];

    GLWidget *openg_widget;

    QGroupBox * serial_box;
    QFormLayout * serial_widget_layout;
    QPushButton * send_command_button;
    QLineEdit * command_input;
    QSerialPort * serial_port;
    QPlainTextEdit * serial_output;
    QLabel * serial_status;

    parse *serial_parse=new parse();
    mpu sensor_data;

public:
    explicit sensors(QWidget *parent = nullptr);
    ~sensors();

    void closeEvent(QCloseEvent *event);

    void set_port(QString port);
    void set_speed(QString speed);
    void close_serial();
    void send_command();
    void read_input(const QByteArray &data);

    void setup_serial_widget();
    void mpu_serial_widget();
    void gps_serial_widget();
    void other_sensor_widget();

    void set_sensors_value();

signals:
};

#endif // SENSORS_WIDGET_H
