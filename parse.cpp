#include "parse.h"

parse::parse() {

}

void parse::get_command(QString input,mpu *mpu_input,gps *gps_input){
    QRegularExpression re("(\\S+): (\\S+), (\\S+), (\\S+)");
    QRegularExpressionMatch match = re.match(input);

    if (match.hasMatch()) {
        if(match.captured(1)=="Accel")
        {
            mpu_input->set_acceleration(match.captured(2),match.captured(3),match.captured(4));
        }else if(match.captured(1)=="Gyro"){
            mpu_input->set_rotation(match.captured(2),match.captured(3),match.captured(4));
        }else if(match.captured(1)=="Magnet"){
            mpu_input->set_magnetics(match.captured(2),match.captured(3),match.captured(4));
        }else if(match.captured(1)=="GPS"){
            gps_input->set_coordinates(match.captured(2),match.captured(3),match.captured(4));
        }
    }else{
        re.setPattern("(\\S+): (\\S+)");
        match = re.match(input);
        if (match.hasMatch()) {

            if(match.captured(1)=="Temp")
            {
                mpu_input->set_tempeture(match.captured(2));
            }else if(match.captured(1)=="Pressure"){
                mpu_input->set_pressure(match.captured(2));
            }else if(match.captured(1)=="Satellites"){
                gps_input->set_satellites(match.captured(2));
            }else if(match.captured(1)=="Accuracy"){
                gps_input->set_accuracy(match.captured(2));
            }else if(match.captured(1)=="Altitude"){
                gps_input->set_altitude(match.captured(2));
            }else if(match.captured(1)=="Speed"){
                gps_input->set_speed(match.captured(2));
            }
        }
    }

}
