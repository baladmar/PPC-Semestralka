# PPC Balaďa Semestrálka

Semestrální práce pro předmět B2B99PPC 

## Úvod

Jednoduchý prototyp grafického rozhrání pro zobrazování dat z seriové komunikace.

## Funkce

* Čtení a zpracování dat ze seriového rozhraní.
* Zobrazení dat z MPU a GPS v widgetech.
* OpenGL okno propojené s MPU sensory

## Požadavky

* Qt6
* qmake
* make
* gcc/MinGW

## Stažení

```
git clone https://gitlab.fel.cvut.cz/baladmar/PPC-Semestralka
```
## Kompilace

```
qmake -o Makefile -Semestralka.pro
make
```

## License
[MIT]
