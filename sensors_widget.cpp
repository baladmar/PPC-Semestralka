#include "sensors_widget.h"

sensors::sensors(QWidget *parent)
    : QWidget{parent}
{
    layout = new QBoxLayout(QBoxLayout::LeftToRight);

    setup_serial_widget();
    mpu_serial_widget();
    gps_serial_widget();

    other_sensor_widget();

    setLayout(layout);
}

void sensors::setup_serial_widget()
{
    serial_widget_layout = new QFormLayout();
    serial_box = new QGroupBox("Serial");

    send_command_button = new QPushButton();
    send_command_button->setText("Send Command");

    command_input = new QLineEdit();
    serial_port = new QSerialPort();
    serial_output = new QPlainTextEdit();

    serial_status = new QLabel("Connected");
    serial_status->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    serial_status->setStyleSheet("QLabel { background-color : green }");

    serial_widget_layout->addRow(command_input,send_command_button);

    serial_output->setReadOnly(1);
    serial_widget_layout->addRow(serial_output);

    serial_widget_layout->addRow(tr("Status:"),serial_status);

    serial_box->setLayout(serial_widget_layout);

    layout->addWidget(serial_box);

    connect(send_command_button,&QPushButton::released,this,&sensors::send_command);

    connect(serial_port,&QSerialPort::errorOccurred,[&](){
        if (serial_port->error() == QSerialPort::ResourceError) {
            qDebug() << serial_port->errorString();
            serial_status->setStyleSheet("QLabel { background-color : red }");
            serial_status->setText("Disconnected");
        }
    });

    connect(serial_port, &QSerialPort::readyRead, [&](){
        QString serial_string=serial_port->readLine();
        read_input(serial_string.toUtf8());
        serial_parse->get_command(serial_string,&sensor_data,&gps_data);
        set_sensors_value();
    });

}

void sensors::mpu_serial_widget()
{
    mpu_box = new QGroupBox("MPU");
    mpu_layout=new QFormLayout();

    mpu_box_gyro = new QGroupBox("Gyroscope");
    mpu_box_accel = new QGroupBox("Accerometer");
    mpu_box_magneto = new QGroupBox("Magnetometer");

    openg_widget = new GLWidget();

    serial_parse=new parse();

    QChar x[3]={'X','Y','Z'};

    for (int var = 0; var < 3; ++var) {
        mpu_widget_layout[var] = new QFormLayout;
        mpu_gyro[var] = new QLineEdit;
        mpu_gyro[var]->setReadOnly(1);
        mpu_widget_layout[0]->addRow(x[var],mpu_gyro[var]);
    }

    for (int var = 0; var < 3; ++var) {
        mpu_accel[var] = new QLineEdit;
        mpu_accel[var]->setReadOnly(1);
        mpu_widget_layout[1]->addRow(x[var],mpu_accel[var]);
    }

    for (int var = 0; var < 3; ++var) {
        mpu_magneto[var] = new QLineEdit;
        mpu_magneto[var]->setReadOnly(1);
        mpu_widget_layout[2]->addRow(x[var],mpu_magneto[var]);
    }

    mpu_box_gyro->setLayout(mpu_widget_layout[0]);
    mpu_box_accel->setLayout(mpu_widget_layout[1]);
    mpu_box_magneto->setLayout(mpu_widget_layout[2]);


    mpu_layout->addWidget(mpu_box_gyro);
    mpu_layout->addWidget(mpu_box_accel);
    mpu_layout->addWidget(mpu_box_magneto);
    mpu_layout->addWidget(openg_widget);

    mpu_box->setLayout(mpu_layout);
    layout->addWidget(mpu_box);
}
void sensors::gps_serial_widget()
{
    gps_widget_layout = new QFormLayout();
    gps_box = new QGroupBox(tr("GPS"));

    latitude_widget = new QLineEdit();
    longitude_widget = new QLineEdit();

    speed_widget= new QLineEdit();
    altitude_widget = new QLineEdit();
    accuracy_widget = new QLineEdit();
    satellites_widget = new QLineEdit();

    latitude_widget->setReadOnly(1);
    longitude_widget->setReadOnly(1);

    gps_widget_layout->addRow("Latitude",latitude_widget);
    gps_widget_layout->addRow("Longitude",longitude_widget);

    gps_widget_layout->addRow("Speed",speed_widget);
    gps_widget_layout->addRow("Altitude",altitude_widget);
    gps_widget_layout->addRow("Accuracy",accuracy_widget);
    gps_widget_layout->addRow("Satellites",satellites_widget);

    gps_box->setLayout(gps_widget_layout);

    layout->addWidget(gps_box);

}

void sensors::set_port(QString port){
    serial_port->setPortName(port);
    serial_port->open(QIODevice::ReadWrite);
    qDebug() << "Port set:" << port;
}

void sensors::set_speed(QString speed){
    bool ok;
    int speed_int = speed.toInt(&ok,10);
    if(ok)
    {
        qDebug() << "Port speed:" << speed_int;
        serial_port->setBaudRate(speed_int);
    }
}

void sensors::close_serial()
{
    if(serial_port->isOpen())
    {
        serial_port->close();
    }
}

void sensors::send_command(){
    QString command = command_input->text();
    qDebug() << command << "\n";
    serial_port -> write(command.toStdString().c_str());
}

void sensors::read_input(const QByteArray &data)
{
    serial_output->centerCursor();
    serial_output->insertPlainText(data);
}

void sensors::other_sensor_widget(){

    other_sensor_box = new QGroupBox(tr("Other"));
    other_sensor_layout = new QFormLayout();

    pressure_widget = new QLineEdit();
    temperature_widget = new QLineEdit();

    pressure_widget->setReadOnly(1);
    temperature_widget->setReadOnly(1);

    other_sensor_layout->addRow("Temperature",temperature_widget);
    other_sensor_layout->addRow("Pressure",pressure_widget);

    other_sensor_box->setLayout(other_sensor_layout);

    layout->addWidget(other_sensor_box);
}

void sensors::closeEvent(QCloseEvent *event)
{
    qDebug() << "Exit";
    close_serial();
    event->accept();
}

void sensors::set_sensors_value(){
    for (int var = 0; var < 3; ++var) {
        mpu_gyro[var]->setText(*sensor_data.rotation[var]);
        mpu_accel[var]->setText(*sensor_data.acceleration[var]);
        mpu_magneto[var]->setText(*sensor_data.magnetic[var]);
    }

    bool *ok = new bool;

    openg_widget->setXRotation(sensor_data.rotation[0]->toFloat(ok)*20);
    openg_widget->setYRotation(sensor_data.rotation[1]->toFloat(ok)*20);
    openg_widget->setZRotation(sensor_data.rotation[2]->toFloat(ok)*20);

    temperature_widget->setText(sensor_data.tempeture);
    pressure_widget->setText(sensor_data.pressure);

    speed_widget->setText(gps_data.speed);
    accuracy_widget->setText(gps_data.accuracy);
    satellites_widget->setText(gps_data.satellites);
    altitude_widget->setText(gps_data.altitude);
    latitude_widget->setText(gps_data.dd_latitude);
    longitude_widget->setText(gps_data.dd_longitude);
}

sensors::~sensors()
{
    close_serial();
}
