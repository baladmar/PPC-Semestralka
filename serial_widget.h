#ifndef SERIAL_WIDGET_H
#define SERIAL_WIDGET_H

#include <QWidget>
#include <QSpinBox>
#include <QPushButton>
#include <QFormLayout>
#include <QComboBox>
#include <QtSerialPort/QSerialPortInfo>
#include <QTimer>


#include "sensors_widget.h"

class serial_widget : public QWidget
{
    Q_OBJECT
    QFormLayout *layout;

    QPushButton *button_connect;
    QComboBox *combo_port,*combo_speed;
    QSerialPortInfo port_info;

    QTimer *port_timer;

public:
    serial_widget(QWidget *parent = nullptr);
    ~serial_widget();
};
#endif // SERIAL_WIDGET_H
