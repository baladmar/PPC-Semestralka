QT       += core gui
QT       += serialport
QT       += opengl
QT       += openglwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    gl_widget.cpp \
    gps.cpp \
    main.cpp \
    model.cpp \
    mpu.cpp \
    parse.cpp \
    sensors_widget.cpp \
    serial_widget.cpp

HEADERS += \
    gl_widget.h \
    gps.h \
    model.h \
    mpu.h \
    parse.h \
    sensors_widget.h \
    serial_widget.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
