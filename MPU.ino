#include "MPU9250.h"
#include <SoftwareSerial.h>
#include <TinyGPS.h>

static const int RXPin = 0, TXPin = 1;
static const uint32_t GPSBaud = 9600;

MPU9250 mpu;

TinyGPS gps;

SoftwareSerial swSerial(RXPin, TXPin);

void setup() {
  Serial.begin(115200);

  swSerial.begin(9600);

  Wire.begin(7, 6);
  delay(2000);

  if (!mpu.setup(0x68)) {
    while (1) {
      Serial.println("MPU connection failed. Please check your connection with `connection_check` example.");
      delay(5000);
    }
  }

  Serial.println("Accel Gyro calibration will start in 5sec.");
  Serial.println("Please leave the device still on the flat plane.");
  mpu.verbose(true);
  delay(3000);
  mpu.calibrateAccelGyro();
  mpu.verbose(false);
  /*
    Serial.println("Mag calibration will start in 5sec.");
    Serial.println("Please Wave device in a figure eight until done.");
    delay(5000);
    mpu.calibrateMag();

    print_calibration();
    mpu.verbose(false);
    */
}

void loop() {
  if (mpu.update()) {
    static uint32_t prev_ms = millis();
    if (millis() > prev_ms + 200) {
      print_gyroscope();
      print_accelerometer();
      print_magnetometer();
      print_temperature();
      print_gps();
      prev_ms = millis();
    }
  }
}

void print_gps() {
  bool newData = false;
  unsigned long chars;
  unsigned short sentences, failed;

  for (unsigned long start = millis(); millis() - start < 1000;) {
    while (swSerial.available()) {
      char c = swSerial.read();
      if (gps.encode(c)) {
        newData = true;
      }
    }
  }

  float latitude, longitude;
  unsigned long old_data;
  gps.f_get_position(&latitude, &longitude, &old_data);
  Serial.print("GPS: ");
  Serial.print(latitude == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : latitude, 6);
  Serial.print(", ");
  Serial.print(longitude == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : longitude, 6);
  Serial.print(", ");
  Serial.println(gps.f_altitude() == TinyGPS::GPS_INVALID_F_ALTITUDE ? 0 : gps.f_altitude());
  Serial.print("Satellites: ");
  Serial.println(gps.satellites() == TinyGPS::GPS_INVALID_SATELLITES ? 0 : gps.satellites());
  Serial.print("Accuracy: ");
  Serial.println(gps.hdop() == TinyGPS::GPS_INVALID_HDOP ? 0 : gps.hdop());
  Serial.print("Speed: ");
  Serial.println(gps.f_speed_kmph() == TinyGPS::GPS_INVALID_F_SPEED ? 0 : gps.f_speed_kmph());
  Serial.println();
}

void print_temperature() {
  Serial.print("Temp: ");
  Serial.println(mpu.getTemperature(), 2);
}

void print_gyroscope() {
  Serial.print("Gyro: ");
  Serial.print(mpu.getYaw(), 2);
  Serial.print(", ");
  Serial.print(mpu.getPitch(), 2);
  Serial.print(", ");
  Serial.println(mpu.getRoll(), 2);
}

void print_accelerometer() {
  Serial.print("Accel: ");
  Serial.print(mpu.getAccX(), 2);
  Serial.print(", ");
  Serial.print(mpu.getAccY(), 2);
  Serial.print(", ");
  Serial.println(mpu.getAccZ(), 2);
}

void print_magnetometer() {
  Serial.print("Magnet: ");
  Serial.print(mpu.getGyroX(), 2);
  Serial.print(", ");
  Serial.print(mpu.getGyroY(), 2);
  Serial.print(", ");
  Serial.println(mpu.getGyroZ(), 2);
}

void print_calibration() {
  Serial.println("< calibration parameters >");
  Serial.println("accel bias [g]: ");
  Serial.print(mpu.getAccBiasX() * 1000.f / (float)MPU9250::CALIB_ACCEL_SENSITIVITY);
  Serial.print(", ");
  Serial.print(mpu.getAccBiasY() * 1000.f / (float)MPU9250::CALIB_ACCEL_SENSITIVITY);
  Serial.print(", ");
  Serial.print(mpu.getAccBiasZ() * 1000.f / (float)MPU9250::CALIB_ACCEL_SENSITIVITY);
  Serial.println();
  Serial.println("gyro bias [deg/s]: ");
  Serial.print(mpu.getGyroBiasX() / (float)MPU9250::CALIB_GYRO_SENSITIVITY);
  Serial.print(", ");
  Serial.print(mpu.getGyroBiasY() / (float)MPU9250::CALIB_GYRO_SENSITIVITY);
  Serial.print(", ");
  Serial.print(mpu.getGyroBiasZ() / (float)MPU9250::CALIB_GYRO_SENSITIVITY);
  Serial.println();
  Serial.println("mag bias [mG]: ");
  Serial.print(mpu.getMagBiasX());
  Serial.print(", ");
  Serial.print(mpu.getMagBiasY());
  Serial.print(", ");
  Serial.print(mpu.getMagBiasZ());
  Serial.println();
  Serial.println("mag scale []: ");
  Serial.print(mpu.getMagScaleX());
  Serial.print(", ");
  Serial.print(mpu.getMagScaleY());
  Serial.print(", ");
  Serial.print(mpu.getMagScaleZ());
  Serial.println();
}