#ifndef PARSE_H
#define PARSE_H

#include "gps.h"
#include <QDebug>
#include <QString>
#include <QRegularExpression>
#include <mpu.h>

class parse
{

public:
    parse();
    void get_command(QString input,mpu *mpu_input,gps *gps_input);

};

#endif // PARSE_H
