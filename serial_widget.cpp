#include "serial_widget.h"

serial_widget::serial_widget(QWidget *parent)
    : QWidget(parent)
{
    layout= new QFormLayout();

    button_connect = new QPushButton("Connect");

    combo_port = new QComboBox();
    combo_speed = new QComboBox();

    QStringList speeds={"9600","115200"};
    combo_speed->setEditable(1);
    combo_speed->addItems(speeds);

    layout->addRow(tr("Port:"),combo_port);
    layout->addRow(tr("Speed:"),combo_speed);
    layout->addWidget(button_connect);

    port_timer= new QTimer();
    port_timer->setInterval(100);
    port_timer->start();

    QObject::connect(port_timer, &QTimer::timeout, [&](){
        for (const QSerialPortInfo &portInfo : QSerialPortInfo::availablePorts()) {
            if(-1==combo_port->findText(portInfo.portName())){
                combo_port->addItem(portInfo.portName());
            }
        }

    });

    QObject::connect(button_connect,&QPushButton::clicked,[&](){
        sensors *x = new sensors;
        qDebug() << combo_port->currentText();
        x->set_port(combo_port->currentText());
        x->set_speed(combo_speed->currentText());
        x->show();
    });

    //connect(x,x::closeEvent,[=](){ x->close_serial(); });

    setLayout(layout);
}

serial_widget::~serial_widget() {
    delete combo_port;
    delete combo_speed;
    delete button_connect;
    delete layout;
}
