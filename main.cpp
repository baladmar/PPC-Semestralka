#include "serial_widget.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    serial_widget w;
    w.show();
    return a.exec();
}
