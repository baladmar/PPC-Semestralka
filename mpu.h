#ifndef MPU_H
#define MPU_H
#include <QString>

class mpu
{
public:
    mpu();

    QString rotation_x="0",rotation_y="0",rotation_z="0";
    QString acceleration_x="0",acceleration_y="0",acceleration_z="0";
    QString magnetic_x="0",magnetic_y="0",magnetic_z="0";

    const QString *rotation[3]={&rotation_x,&rotation_y,&rotation_z};
    const QString *acceleration[3]={&acceleration_x,&acceleration_y,&acceleration_z};
    const QString *magnetic[3]={&magnetic_x,&magnetic_y,&magnetic_z};

    QString pressure="0",tempeture="0";

    void set_rotation(QString x,QString y,QString z);
    void set_acceleration(QString x,QString y,QString z);
    void set_magnetics(QString x,QString y,QString z);
    void set_pressure(QString input);
    void set_tempeture(QString input);

};

#endif // MPU_H
